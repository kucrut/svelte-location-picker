# @kucrut/svelte-location-picker

## 0.2.1

### Patch Changes

- 2bffd6c: CI: Fix publish_package script

## 0.2.0

### Minor Changes

- ed2fb24: Fix packaging

## 0.1.5

### Patch Changes

- aec5b21: Update dependencies

## 0.1.4

### Patch Changes

- 66173fe: CI: Fix pages

## 0.1.3

### Patch Changes

- a7eba6f: Improve demo page

## 0.1.2

### Patch Changes

- 0a3a713: Improve release flow

## 0.1.1

### Patch Changes

- a454774: CI: Fix release job

## 0.1.0

### Minor Changes

- 4c08058: Improve accessibility on Modal & Dialog components (uses actions from @kucrut/svelte-stuff)

## 0.0.8

### Patch Changes

- acfc658: Migrate packaging

## 0.0.7

### Patch Changes

- 75b130a: Update dependencies (20221203)

## 0.0.6

### Patch Changes

- 4769075: Add more data to package.json

## 0.0.5

### Patch Changes

- e57eede: Publish flow 9

## 0.0.4

### Patch Changes

- e47ae6d: Publish flow 8

## 0.0.3

### Patch Changes

- 18c11be: Publish flow 7

## 0.0.2

### Patch Changes

- f2db173: CI: Use kucrut/pnpm image
- 9c04328: Publish flow 6
- 8f1d07e: Publish flow
- ce7ccaa: Add publish flow via GitLab CI
