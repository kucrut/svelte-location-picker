# Svelte Location Picker

Simple location picker for Svelte projects.

See [demo and code sample](https://kucrut.gitlab.io/svelte-location-picker).
